## Lilypad ESP32

This board is designed to fit into, and update the lilypad family.
The board is based around espressif's ESP32-WROOM-32. 

Development is being completed by Harry Iliffe at FabLabWGTN, NZ.
